import numpy as np
from utils import generate_jobs_sample, generate_machines_sample
from copy import deepcopy

class heuristique:
    def __init__(self, jobs, machines) -> None:
        self.jobs = jobs
        self.machines = machines
        # self.algo = algo
    
    def SPT(self):
        '''
        jobs are sorted in increasing order of thier total processing time.
        Returns:
            allocation (list): list of tasks sorted by total processing time.
        '''
        jobs = deepcopy(self.jobs)
        jobs.sort(key=lambda job: sum([job[i][1] for i in range(3)]) )
        allocation = []
        for job in jobs:
            for i in range(3):
                allocation.append(job[i])
        return allocation

    def SPT2(self):
        '''
        jobs are sorted in increasing order of thier total processing time.
        Returns:
            allocation (list): list of tasks sorted by first operation time.
        '''
        jobs = deepcopy(self.jobs)
        jobs.sort(key=lambda job: job[0][1] )
        allocation = []
        for job in jobs:
            for i in range(3):
                allocation.append(job[i])
        return allocation

    def WSPT(self):
        '''
        jobs are sorted in increasing order of thier total processing time.
        Returns:
            allocation (list): list of tasks sorted by total processing time / weight.
        '''
        jobs = deepcopy(self.jobs)
        jobs.sort(key=lambda job: sum([job[i][1]/job[-1] for i in range(3)]) )
        allocation = []
        for job in jobs:
            for i in range(3):
                allocation.append(job[i])
        return allocation

    def EDD(self):
        '''
        jobs are sorted in increasing order of thier total processing time.
        Returns:
            allocation (list): list of tasks sorted by deadline.
        '''
        jobs = deepcopy(self.jobs)
        jobs.sort(key=lambda job: job[-2])
        allocation = []
        for job in jobs:
            for i in range(3):
                allocation.append(job[i])
        return allocation