import numpy as np

def generate_jobs_sample(n_jobs=5):
    """
    Generate a n random jobs.
    A job is a list of 3 * (machine index, time of the task, job_index) and two integers, one 
    that represents the time after which a job is considered late and one that represents the cost of
    not finishing the job.
    Paremeters:
        n_jobs (int): number of jobs to generate
    Returns:
        jobs (list): list of jobs generated 
    """
    jobs = []
    for i in range(n_jobs):
        machines_assignement = np.random.permutation([1, 2, 3]) # random machine assignement order
        task_times = np.random.randint(1, 6, size=3) # time taken by each task 
        due_date = np.sum(task_times) + int(np.random.randint(15, size=1)) # if the job finishes after that date, it is considered late
        cost = np.random.randint(1, 10)
        job = list(zip(machines_assignement, task_times, [i] * 3)) + [due_date, cost]
        jobs.append(job)
    return jobs

def generate_machines_sample():
    """
    Generates 3 random machines.
    Each machine is described by a tuple composed of the starting time and the 
    ending time of the breakdown on the machine
    Returns:
        machines (list): list of machines generated
    """
    machines = []
    for _ in range(3):
        start_time = np.random.randint(2, 10) # starting time of the breakdown
        end_time = start_time + np.random.randint(1, 5) # end of the breakdown on the machine
        machines.append((start_time, end_time))
    return machines 

def toCplexDat(jobs,n_jobs,machines):

    nbJobsLine = "nbJobs = " + str(n_jobs+3) + ";"  #we sum 3 to consider the 3 holes in the machines
    dueDateLine = "due = ["
    weigthsLine = "weigths = ["
    opsLine = "Ops = [\n"
    startDateLine = "startDate = ["

    for job in jobs:
        dueDateLine   += str(job[3]) +","
        weigthsLine   += str(job[4]) +","
        startDateLine += "0,"
        opsLine += "[ <" + str(job[0][0]) +","+ str(job[0][1]) +">, "
        opsLine +=   "<" + str(job[1][0]) +","+ str(job[1][1]) +">, "
        opsLine +=   "<" + str(job[2][0]) +","+ str(job[2][1]) +">],\n"

    i = 1
    for machine in machines:
        if i == 1:
            opsLine +=   "[ <1,"+ str(machine[1] - machine[0]) +">, "
            opsLine +=   "<2,0>, "
            opsLine +=   "<3,0>],\n"

            startDateLine += str(machine[0]) + ","
            dueDateLine += str(machine[1]) + ","

        if i == 2:
            opsLine +=   "[ <1,0>, "
            opsLine +=   "<2," + str(machine[1] - machine[0]) +">, "
            opsLine +=   "<3,0>],\n"

            startDateLine += str(machine[0]) + ","
            dueDateLine += str(machine[1]) + ","

        if i == 3:
            opsLine +=   "[ <1,0>, "
            opsLine +=   "<2,0>, "
            opsLine +=   "<3," + str(machine[1] - machine[0]) +">],"

            startDateLine += str(machine[0]) + ","
            dueDateLine += str(machine[1]) + ","
        i += 1

        weigthsLine += "100,"

    dueDateLine = dueDateLine[:-1] + "];" 
    weigthsLine = weigthsLine[:-1] + "];" 
    opsLine = opsLine[:-1] + "];" 
    startDateLine = startDateLine[:-1] + "];" 


    lines = [
        "nbMachines = 3;",
        nbJobsLine,
        dueDateLine,
        weigthsLine,
        opsLine,
        startDateLine
    ]

    with open('jobs.dat', 'w') as f:
        f.write('\n'.join(lines))